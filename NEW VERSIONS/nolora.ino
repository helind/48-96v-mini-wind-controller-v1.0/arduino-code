#include <Arduino.h>
#include <U8g2lib.h>
//#include <TimerFive.h>
#include <PinChangeInterrupt.h>
#define PCINT_ENABLE_MANUAL
#define PCINT_ENABLE_PCINT4
#define PCINT_ENABLE_PCINT5
#define PCINT_ENABLE_PCINT6
#define PCINT_ENABLE_PCINT7
#include <HELIND_LCD.h>
#include <EEPROM.h>
#include <ModbusRtu.h>
#include <ModbusMaster.h>
#include "avr/wdt.h"

//#define FAC_DEF
enum modbus_slave_data {
	mb_wspeed,
	mb_voltage,
	mb_current,
	mb_w_l,
	mb_w_h,
	mb_wh_l,
	mb_wh_h,
	mb_temp_l,
	mb_temp_h,
	mb_cur_state,
	mb_sens_err,
	mb_volt_err,
	mb_fan_state,
	mb_vmax,
	mb_vmin,
	mb_wmax,
	mb_wmin,
	mb_brmenustate,
	mb_pbrmenustate,
	mb_loramodestate,
	mb_loramenustate,
	mb_braketimer,
	mb_loratimer,
	mb_wattmeter_toggle,
	mb_windsensor_toggle,
	mb_wattmeter_shuntval,
	mb_wattmeter_id,
	mb_slave_id,
	mb_data_size
};

// Serial1 olacaq
#define sensor_serial Serial1
const uint8_t sensor_rs_de = 26;
const unsigned long sensor_rs_baud = 9600;
const unsigned long wind_sensor_read_period = 1000;
unsigned long wind_sensor_query_period = 10000;
unsigned long wind_sensor_poll_period = 0;
bool wind_sensor_poll_flag = false;
uint8_t wind_sensor_error_count = 0;
uint8_t max_sensor_error_count = 4;
uint16_t modbus_poll_data[mb_data_size];
uint16_t old_modbus_poll_data[mb_data_size];
bool breakCond = false;

#define modbus_serial Serial2
const uint8_t modbus_rs_de = 27;
const long modbus_rs_baud = 9600;
bool after_pb_braking = false;
enum pins {
	menu_btn_pin = 10,
	up_btn_pin = 11,
	down_btn_pin = 12,
	ok_btn_pin = 13,
	backlight_pin = 37,
	voltage_read_pin = A1,
	braking_pin = 31,
	braking_led = 48,
	phase_braking_pin = 30,
	phase_braking_led = 47,
	charging_pin = 32,
	charging_led = 49,
	temp1_pin = A0,
	temp2_pin = A2,
	fan1_pin = 28,
	fan2_pin = 29,
	buzzer_pin = 36,
	lora_pin = A5,
	pwm_pin = 44
};

enum val_list { val_VOLTAGE, val_CURRENT, val_W, val_KWH, val_TEMP };

#define min_voltage_limit max_min_array[1]
#define max_voltage_limit max_min_array[0]
#define min_wind_limit max_min_array[3]
#define max_wind_limit max_min_array[2]

#define OPAMP_MAX_VOLTAGE 115

#define LMODETEXT "48"
#define HMODETEXT "96"

#define AUTO_MODE_DIFF_VOLTAGE 85

#define BAT_LVOLTAGE_LOW 48.0
#define BAT_LVOLTAGE_HIGH 58.8

#define BAT_HVOLTAGE_LOW 96.0
#define BAT_HVOLTAGE_HIGH 106.8

#define BAT_LLEVELVOLTAGE_LOW 46.0
#define BAT_LLEVELVOLTAGE_HIGH 80.0

#define BAT_HLEVELVOLTAGE_LOW 96.0
#define BAT_HLEVELVOLTAGE_HIGH 116.0

#define FAN_LIM_TEMP 40
#define MIN_TEMP_LIM 35

#define mapVal1 339.81
#define mapVal2 472.48
#define mapVal3 45.00
#define mapVal4 62.25

const unsigned long backlight_off_period = 20000;
unsigned long backlight_timer = 0;

uint8_t VOLTAGE_MODE = 0;
uint8_t wind_sensor_state,watt_meter_state;
uint8_t save_flag;
uint8_t rs_de = 0;
bool phase_braking = false;
bool braking = false;
bool voltage_error;
bool wind_sensor_error;
bool flashing_bool;
bool first_start = true;
uint8_t fan_state = 0;
uint8_t _selected_modbus_id = 5;
unsigned long flashing_interval = 1000;
unsigned long flashing_period = 0;
unsigned long wperiod = 0;
bool temp_error = false;
bool wattmeter_error = false;
// Modbus sensor_master(0, sensor_serial, sensor_rs_de);
ModbusMaster mb_master;
Modbus slave(slave_id, modbus_serial, modbus_rs_de);

unsigned long debouncing_time = 300;  // Debouncing Time in Milliseconds
volatile unsigned long last_millis = 301;

#define _debnc_begin if ((millis() - last_millis) >= debouncing_time)
#define _debnc_end last_millis = millis();

bool wsensqueue = false;
bool wmeterqueue = true;

bool brakestart = false, phasebrakestart = false;
unsigned long pb_after_brake_period = 0;
uint16_t max_samples = 64;
uint16_t sample_count = 0;
unsigned long voltage_sample_period = 0;
unsigned long max_sample_period = 1;
float sampled_voltage = 0;
uint32_t voltage_sample_total = 0;
bool instant = true;
bool after_error = false;
int ssval = 0;
volatile bool scanneeded = false;
volatile bool wattmeter_set_needed = false;
bool lora_pb_flag = false;
bool lora_res_flag = false;
bool lora_braking = false;
bool autoresbreak = false, autopbreak = false;
bool brake_before_pb = false;
unsigned long lora_pb_period = 0;
unsigned long max_lora_pb_period = 20000;
unsigned long lora_res_period = 0;
unsigned long max_lora_res_period = 20000;

enum MENU_PAGES_DEF {
	PERF_MENU,
	SENSOR_MENU,
	PARAM_MENU,
	MODBUS_MENU,
	WATTMETER_MENU,
	BRAKING_MENU
};

bool uint8_to_bool(uint8_t val) {
	if (val != 0)
		return true;
	return false;
}

void setup(void) {
	
	noInterrupts();
	uiSetup_page1();
	set_interrupts();
	interrupts();
	pin_config();
	u8g.firstPage();

	lcd_backlight(backlight_pin, true);
	startup_config();
	//Timer5.initialize(59);
	wdt_enable(WDTO_4S);
	
}

void loop() {

	update_slave_data();
	slave.setID(slave_id);
	slave.poll(modbus_poll_data, mb_data_size);
	update_own_data();
	fan_control();
	show_menu_page(menu_page);
	if (error_mode())
		flash_errors();

	lcd_backlight_control(backlight_off_period);
	if (save_flag && select == 0) {
		eeprom_control("SAVE");
		save_flag = false;
	}
	brake_loop();

	if (uint8_to_bool(watt_meter) && !uint8_to_bool(wind_sensor)) {
		wmeterqueue = true;
		wsensqueue = false;
	}
	else if (!uint8_to_bool(watt_meter) && uint8_to_bool(wind_sensor)) {
		wmeterqueue = false;
		wsensqueue = true;
	}

	if (uint8_to_bool(watt_meter)) {
		if (wmeterqueue) {
			if (millis() - wperiod >= 500) {
				wperiod = millis();
				update_wattmeter_vals();
				wmeterqueue = false;
				wsensqueue = true;
			}
		}
	}
	if (scanneeded)
		wattmeterScanAction();
	if (wattmeter_set_needed)
		setWattmeterShunt(wattmeter_shunt_val);
	// Serial.println(wattmeter_slave_id);
	wdt_reset();
}

void setWattmeterShunt(uint8_t val) {
	mb_master.writeSingleRegister(3, val);
	wdt_reset();
	delay(500);
	wdt_reset();
	wattmeter_set_needed = false;
	save_flag = 1;
}

void menu() {
	lcd_backlight(backlight_pin, true);
	reset_backlight_timer();
	_debnc_begin{
	  menu_current = 0;
	  if (select != 0) {
		eeprom_control("READ");
		select = 0;
	  }
	  switch (menu_page) {
		case PERF_MENU:
		  menu_page = SENSOR_MENU;
		  break;
		case SENSOR_MENU:
		  menu_page = PARAM_MENU;
		  break;
		case PARAM_MENU:
		  menu_page = MODBUS_MENU;
		  break;
		case MODBUS_MENU:
		  menu_page = BRAKING_MENU;
		  break;
		case WATTMETER_MENU:
			menu_page = MODBUS_MENU; break;
		case BRAKING_MENU:
		  menu_page = PERF_MENU;
		  break;
	  }

	  _debnc_end
	}
}

void up() {
	lcd_backlight(backlight_pin, true);
	reset_backlight_timer();
	_debnc_begin{
	  if (menu_page == SENSOR_MENU) {
		menu_current--;
		if (menu_current < 0)
		  menu_current = PERF_MENU_ITEMS - 1;
	  }
   else if (select != 0 && menu_page == PARAM_MENU) {
  if (menu_current == 0 || menu_current == 2)
	max_min_array[menu_current] += 0.1;
  else if (menu_current == 1 && min_voltage_limit < max_voltage_limit - 0.2)
	max_min_array[menu_current] += 0.1;
  else if (menu_current == 3 && min_wind_limit < max_wind_limit - 0.2)
	max_min_array[menu_current] += 0.1;
}
else if (menu_page == PARAM_MENU && select == 0) {
if (menu_current <= 0)
  menu_current = MENU_ITEMS - 1 - (!wind_sensor) * 2;
else
  menu_current--;
}
else if (menu_page == MODBUS_MENU && select == 0) {
if (menu_current <= 0)
  menu_current = 1;
else
  menu_current--;
}
else if (menu_page == MODBUS_MENU && select != 0) {
slave_id++;
if (slave_id >= 247 || slave_id == 0)
  slave_id = 1;
}
else if (menu_page == WATTMETER_MENU && select == 0) {
menu_current--;
if (menu_current < 0)
  menu_current = 2;
}
else if (menu_page == WATTMETER_MENU && select != 0 & menu_current == 2) {
wattmeter_shunt_val++;
if (wattmeter_shunt_val > 3)
  wattmeter_shunt_val = 0;
}
else if (menu_page == BRAKING_MENU) {
if (select == 0) {
  menu_current--;
  if (menu_current < 0)
	menu_current = LAST_ENT - 1 - (loramode == PARAMOFF) * 2;
}
else {
switch (menu_current) {
  case BRAKETIMER:
	brake_timer++;
	break;
  case LORATIMER:
	lora_timer++;
	break;
}
}
}
_debnc_end
	}
}

void down() {
	lcd_backlight(backlight_pin, true);
	reset_backlight_timer();
	_debnc_begin{
	  if (menu_page == SENSOR_MENU) {
		menu_current++;
		if (menu_current > PERF_MENU_ITEMS - 1)
		  menu_current = 0;
	  }
   else if (menu_page == PARAM_MENU && select != 0) {
  if (max_min_array[menu_current] > 0.2) {
	if (menu_current == 1 || menu_current == 3)
	  max_min_array[menu_current] -= 0.1;
	else if (menu_current == 0 &&
			 min_voltage_limit < max_voltage_limit - 0.2)
	  max_min_array[menu_current] -= 0.1;
	else if ((menu_current == 2) && (min_wind_limit < max_wind_limit - 0.2))
	  max_min_array[menu_current] -= 0.1;
  }
}
else if (menu_page == PARAM_MENU && select == 0) {
if (menu_current >= MENU_ITEMS - 1 - (!wind_sensor) * 2)
  menu_current = 0;
else
  menu_current++;
}
else if (menu_page == MODBUS_MENU && select == 0) {
if (menu_current >= 1)
  menu_current = 0;
else
  menu_current++;
}
else if (menu_page == MODBUS_MENU && select != 0) {
slave_id--;
if (slave_id == 0)
  slave_id = 247;
}
else if (menu_page == WATTMETER_MENU && select == 0) {
menu_current++;
if (menu_current >= 3)
  menu_current = 0;
}
else if (menu_page == WATTMETER_MENU && select != 0 & menu_current == 2) {
wattmeter_shunt_val--;
if (wattmeter_shunt_val == 255)
  wattmeter_shunt_val = 3;
}
else if (menu_page == BRAKING_MENU) {
if (select == 0) {
  menu_current++;
  if (menu_current > LAST_ENT - 1 - (loramode == PARAMOFF) * 2)
	menu_current = 0;
}
else {
switch (menu_current) {
  case BRAKETIMER:
	brake_timer--;
	break;
  case LORATIMER:
	lora_timer--;
	break;
}
}
}
_debnc_end
	}
}

void ok() {
	if (digitalRead(backlight_pin) == HIGH) {
		lcd_backlight(backlight_pin, true);
		reset_backlight_timer();
	}
	else {
		_debnc_begin{
		  if (menu_page == PERF_MENU) {
			switch (uint8_to_bool(watt_meter)) {
			  case true:
				selected_value++;
				if (selected_value > 4)
				  selected_value = 0;
				break;
			  case false:
				if (selected_value == val_VOLTAGE)
				  selected_value = val_TEMP;
				else
				  selected_value = val_VOLTAGE;
				break;
			}
		  }
	 else if (menu_page == SENSOR_MENU) {
	switch (menu_current) {
	  case 0:
		bitToggle(wind_sensor, 0);
		break;
	  case 1:
		bitToggle(watt_meter, 0);
		selected_value = 0;
		break;
	  case 2:
		bitToggle(wind_al, 0);
		break;
	  case 3:
		bitToggle(voltage_al, 0);
		break;
	}
	save_flag = true;
  }
else if (menu_page == PARAM_MENU) {
select ^= 1;
save_flag = true;
}
else if (menu_page == MODBUS_MENU) {
if (menu_current == 0) {
  select ^= 1;
  save_flag = true;
}
if (menu_current == 1) {
  menu_current = 0;
  menu_page = 4;
}
}
else if (menu_page == WATTMETER_MENU && menu_current == 0) {
scanneeded = true;
}
else if (menu_page == WATTMETER_MENU && menu_current == 2) {
select ^= 1;
if (select == 0)
  wattmeter_set_needed = true;
}
else if (menu_page == BRAKING_MENU) {
switch (menu_current) {
  case LORAMODE:
	loramode++;
	if (loramode > PARAMOFF)
	  loramode = PARAMON;
	break;
  case BRSTATE:
	brstate++;
	if (brstate > PARAMAUTO)
	  brstate = PARAMON;
	break;
  case PBRSTATE:
	pbrstate++;
	if (pbrstate > PARAMAUTO)
	  pbrstate = PARAMON;
	break;
  case LOBSTATE:
	lobstate++;
	if (lobstate > PARAMAUTO)
	  lobstate = PARAMON;
	break;
  case LORATIMER:
  case BRAKETIMER:
	select ^= 1;
	break;
}
save_flag = true;
}
breakCond = false;
_debnc_end
		}
	}
}

void wattmeterScanAction() {
	uint8_t status = 0, cur_id = 0;
	breakCond = true;
	for (uint8_t wmeter_id = 1; wmeter_id <= 255 && breakCond; wmeter_id++) {
		rs_de = sensor_rs_de;
		cur_id = wmeter_id;
		sensor_serial.begin(9600, SERIAL_8N2);
		mb_master.begin(wmeter_id, sensor_serial);
		uint8_t status = mb_master.readHoldingRegisters(0, 4);
		wdt_reset();

		if (status == mb_master.ku8MBSuccess) {
			wattmeter_slave_id = wmeter_id;
			wattmeter_shunt_val = mb_master.getResponseBuffer(3);
			save_flag = 1;
			menu_page = 4;
			break;
		}
		u8g.firstPage();
		do {
			char bff[10] = { 0 };
			sprintf(bff, "%d", cur_id);
			u8g.drawStr(0, 0, "Scanning ID: ");
			u8g.drawStr(0 + u8g.getStrWidth("Scanning ID : ") + 4, 0, bff);
		} while (u8g.nextPage());
		delay(500);
	}
	breakCond = false;
	menu_page = 4;
	scanneeded = false;
}

bool resbreakcondition() {}

bool enablephasebreakcondition() {
	return ((get_wind_speed() > max_wind_limit) || (wind_sensor_state == 0));
}
bool disablephasebreakcondition() {
	return ((get_wind_speed() < min_wind_limit) && (wind_sensor_state == 1));
}

bool enableresbreakcondition() {
	return (get_voltage() > max_voltage_limit);
}
bool disableresbreakcondition() {
	return (get_voltage() < min_voltage_limit);
}

void filter_voltage() {}

void set_factory_values() {
	for (int i = 0; i < 2; i++) {
		VOLTAGE_MODE = i;
		max_voltage_limit = (VOLTAGE_MODE == 0) ? 58.0 : 110.0;
		min_voltage_limit = (VOLTAGE_MODE == 0) ? 56.0 : 90.0;

		max_wind_limit = 4.0;
		min_wind_limit = 2.0;
		wind_sensor = 0;
		voltage_al = 0;
		wind_al = 0;
		watt_meter = 0;
		slave_id = 1;
		wattmeter_slave_id = 2;
		wattmeter_shunt_val = 0;
		pbrstate = PARAMAUTO;
		brstate = PARAMAUTO;
		loramode = PARAMOFF;
		brake_timer = 15;
		lora_timer = 0;
		eeprom_control("SAVE");
	}
}

void show_menu_page(uint8_t _menu_page) {
	u8g.firstPage();
	do {
		switch (_menu_page) {
		case PERF_MENU:
			draw_performance_menu();
			break;
		case SENSOR_MENU:
			drawSensorFull();
			break;
		case PARAM_MENU:
			if (wind_sensor)
				drawMenuFull();
			else
				drawMenu();
			break;
		case MODBUS_MENU:
			modbus_conf_screen();
			break;
		case WATTMETER_MENU:
			wattmeter_conf_screen();
			break;
		case BRAKING_MENU:
			drawBrakingMenu();
			break;
		}
	} while (u8g.nextPage());
}

void set_interrupts() {
	attachPCINT(digitalPinToPCINT(menu_btn_pin), menu, FALLING);
	attachPCINT(digitalPinToPCINT(up_btn_pin), up, FALLING);
	attachPCINT(digitalPinToPCINT(down_btn_pin), down, FALLING);
	attachPCINT(digitalPinToPCINT(ok_btn_pin), ok, FALLING);
	// attachPCINT(digitalPinToPCINT(ok_btn_pin), ok, FALLING);
}

void pin_config() {
	pinMode(backlight_pin, OUTPUT);

	pinMode(menu_btn_pin, INPUT);
	pinMode(up_btn_pin, INPUT);
	pinMode(down_btn_pin, INPUT);
	pinMode(ok_btn_pin, INPUT);

	pinMode(braking_pin, OUTPUT);
	pinMode(braking_led, OUTPUT);
	pinMode(phase_braking_pin, OUTPUT);
	pinMode(phase_braking_led, OUTPUT);
	pinMode(charging_pin, OUTPUT);
	pinMode(A5, OUTPUT);
	digitalWrite(A5, HIGH);
	pinMode(charging_led, OUTPUT);
	pinMode(fan1_pin, OUTPUT);
	pinMode(fan2_pin, OUTPUT);
	pinMode(sensor_rs_de, OUTPUT);
	pinMode(modbus_rs_de, OUTPUT);
	digitalWrite(sensor_rs_de, LOW);
	digitalWrite(modbus_rs_de, LOW);
	//pinMode(pwm_pin, OUTPUT);

}

void lcd_backlight(uint8_t _backlight_pin, bool swch) {
	digitalWrite(_backlight_pin, !swch);
}

void reset_backlight_timer() {
	backlight_timer = millis();
}

void lcd_backlight_control(unsigned long backlight_period) {
	if (millis() - backlight_timer >= backlight_period) {
		lcd_backlight(backlight_pin, false);
		menu_page = 0;
		selected_value = val_VOLTAGE;
	}
}

void eeprom_control(char* action) {
	struct mode_eeprom_addresses {
		void recalc() {
			watt_meter_address = wind_sensor_address + sizeof(uint8_t);
			wind_alarm_address = watt_meter_address + sizeof(uint8_t);
			volt_alarm_address = wind_alarm_address + sizeof(uint8_t);
			min_voltage_address = volt_alarm_address + sizeof(uint8_t);
			max_voltage_address = min_voltage_address + sizeof(float);
			min_wind_address = max_voltage_address + sizeof(float);
			max_wind_address = min_wind_address + sizeof(float);
			;
			slave_id_address = max_wind_address + sizeof(float);
			;
			wattmeter_id_address = slave_id_address + sizeof(uint8_t);
			wattmeter_shunt_address = wattmeter_id_address + sizeof(uint8_t);
			brake_timer_address = wattmeter_shunt_address + sizeof(uint8_t);
			lora_timer_address = brake_timer_address + sizeof(uint8_t);
			brakemenuflags_address = lora_timer_address + sizeof(uint8_t);
		};
		uint8_t wind_sensor_address;
		uint8_t watt_meter_address = wind_sensor_address + sizeof(uint8_t);
		uint8_t wind_alarm_address = watt_meter_address + sizeof(uint8_t);
		uint8_t volt_alarm_address = wind_alarm_address + sizeof(uint8_t);
		uint8_t min_voltage_address = volt_alarm_address + sizeof(uint8_t);
		uint8_t max_voltage_address = min_voltage_address + sizeof(float);
		uint8_t min_wind_address = max_voltage_address + sizeof(float);
		uint8_t max_wind_address = min_wind_address + sizeof(float);
		uint8_t slave_id_address = max_wind_address + sizeof(float);
		;
		uint8_t wattmeter_id_address = slave_id_address + sizeof(uint8_t);
		uint8_t wattmeter_shunt_address = wattmeter_id_address + sizeof(uint8_t);
		uint8_t brake_timer_address = wattmeter_shunt_address + sizeof(uint8_t);
		uint8_t lora_timer_address = brake_timer_address + sizeof(uint8_t);
		uint8_t brakemenuflags_address = lora_timer_address + sizeof(uint8_t);
	};
	mode_eeprom_addresses vlower, vhigher;
	vlower.wind_sensor_address = 0;
	vlower.recalc();
	vhigher.wind_sensor_address =
		vlower.wattmeter_shunt_address + sizeof(float) + 1;
	vhigher.recalc();
	mode_eeprom_addresses* mn;
	switch (VOLTAGE_MODE) {
	case 0:
		mn = &vlower;
		//Serial.println("LOWER MODE SELECTED");
		break;
	case 1:
		mn = &vhigher;
		//Serial.println("HIGHER MODE SELECTED");
		break;
	}

	if (strstr(action, "READ")) {
		//Serial.println("READING FROM EEPROM");
		EEPROM.get(mn->wind_sensor_address, wind_sensor);
		EEPROM.get(mn->watt_meter_address, watt_meter);
		EEPROM.get(mn->wind_alarm_address, wind_al);
		EEPROM.get(mn->volt_alarm_address, voltage_al);
		EEPROM.get(mn->min_voltage_address, min_voltage_limit);
		EEPROM.get(mn->max_voltage_address, max_voltage_limit);
		EEPROM.get(mn->min_wind_address, min_wind_limit);
		EEPROM.get(mn->max_wind_address, max_wind_limit);
		EEPROM.get(mn->slave_id_address, slave_id);
		EEPROM.get(mn->wattmeter_id_address, wattmeter_slave_id);
		EEPROM.get(mn->wattmeter_shunt_address, wattmeter_shunt_val);
		EEPROM.get(mn->brake_timer_address, brake_timer);
		EEPROM.get(mn->lora_timer_address, lora_timer);
		uint8_t brakemenuflags = 0;
		EEPROM.get(mn->brakemenuflags_address, brakemenuflags);
		pbrstate = (brakemenuflags >> 6);
		brstate = (brakemenuflags >> 4) & 0x3;
		loramode = (brakemenuflags >> 2) & 0x3;
		lobstate = (brakemenuflags >> 0) & 0x3;
	}
	if (strstr(action, "SAVE")) {
		//Serial.println("SAVING TO EEPROM");
		EEPROM.put(mn->wind_sensor_address, wind_sensor);
		EEPROM.put(mn->watt_meter_address, watt_meter);
		EEPROM.put(mn->wind_alarm_address, wind_al);
		EEPROM.put(mn->volt_alarm_address, voltage_al);
		EEPROM.put(mn->min_voltage_address, min_voltage_limit);
		EEPROM.put(mn->max_voltage_address, max_voltage_limit);
		EEPROM.put(mn->min_wind_address, min_wind_limit);
		EEPROM.put(mn->max_wind_address, max_wind_limit);
		EEPROM.put(mn->slave_id_address, slave_id);
		EEPROM.put(mn->wattmeter_id_address, wattmeter_slave_id);
		EEPROM.put(mn->wattmeter_shunt_address, wattmeter_shunt_val);
		EEPROM.put(mn->brake_timer_address, brake_timer);
		EEPROM.put(mn->lora_timer_address, lora_timer);
		uint8_t brakemenuflags = 0;
		brakemenuflags |=
			(pbrstate << 6) | (brstate << 4) | (loramode << 2) | (lobstate << 0);
		EEPROM.put(mn->brakemenuflags_address, brakemenuflags);
	}
}

void master_pre_tranmission() {

	digitalWrite(rs_de, HIGH);
		delay(10);
}

void master_post_transmission() {
	digitalWrite(rs_de, LOW);
}

void fan_control() {
	float temp_tt = get_temperature();
	if (!temp_error) {
		if (temp_tt > FAN_LIM_TEMP) {
			digitalWrite(fan1_pin, HIGH);
			fan_state = 1;
		}
		else if (temp_tt <= MIN_TEMP_LIM) {
			digitalWrite(fan1_pin, LOW);
			fan_state = 0;
		}
	}
	else {
		digitalWrite(fan1_pin, LOW);
		fan_state = 0;
	}
}

void brake_loop() {
	switch (brstate) {
	case PARAMAUTO:
		braking_func();
		break;
	case PARAMOFF:
		braking = false;
		break;
	case PARAMON:
		braking = true;
	}
	switch (pbrstate) {
	case PARAMAUTO:
		phase_braking_func();
		break;
	case PARAMOFF:
		phase_braking = false;
		break;
	case PARAMON:
		phase_braking = true;
	}
	if (loramode == PARAMON) {

	if (lobstate == PARAMAUTO) {
		lora_algo(braking || phase_braking || lora_pb_flag || after_pb_braking);
	}
	else {
		lora_algo(lobstate == PARAMON);
	}
}
	else {
		lora_algo(false);
	}

	phase_braking_algo(phase_braking);

	braking_algo(braking);

	charging_algo(charging());
}

void charging_algo(bool act) {
	if (act) {
		digitalWrite(charging_pin, HIGH);
		digitalWrite(charging_led, HIGH);
	}
	else {
		digitalWrite(charging_pin, LOW);
		digitalWrite(charging_led, LOW);
	}
}

uint16_t pwmval = 50;
unsigned long pwm_period = 200;
unsigned long pwm_millis = 0;
void braking_algo(bool act) {
	if (act) {
		digitalWrite(braking_pin, HIGH);
		digitalWrite(braking_led, HIGH);
		//Timer5.pwm(pwm_pin, pwmval);
		if (millis() - pwm_millis >= pwm_period) {
			pwm_millis = millis();
			pwmval++;
			if (pwmval > 1023)pwmval = 1023;
		}
	}
	else {
		digitalWrite(braking_pin, LOW);
		digitalWrite(braking_led, LOW);
		//Timer5.disablePwm(pwm_pin);
		pwmval = 50;
	}
}

void phase_braking_algo(bool act) {
	if (act) {
		digitalWrite(phase_braking_pin, HIGH);
		digitalWrite(phase_braking_led, HIGH);
	}
	else {
		digitalWrite(phase_braking_pin, LOW);
		digitalWrite(phase_braking_led, LOW);
	}
}

void lora_algo(bool act) {
	digitalWrite(lora_pin, !act);
}

void update_slave_data() {



	modbus_poll_data[mb_voltage] = get_voltage() * 10;
	if (!uint8_to_bool(wind_sensor))
		modbus_poll_data[mb_wspeed] = 0;
	else if (wind_sensor_state == 0)
		modbus_poll_data[mb_wspeed] = 0;
	else {
		modbus_poll_data[mb_wspeed] = get_wind_speed() * 10;
	}
	if (!uint8_to_bool(watt_meter)) {
		for (uint8_t i = mb_current; i <= mb_wh_l; i++) {
			modbus_poll_data[i] = 0;
		}
	}
	float tmpf = get_temperature() * 10;
	uint32_t tmp = tmpf;
	modbus_poll_data[mb_temp_h] = tmp >> 16;
	modbus_poll_data[mb_temp_l] = tmp & 0xFFFF;
	if (temp_error) {
		modbus_poll_data[mb_temp_h] = 0;
		modbus_poll_data[mb_temp_l] = 0;
	}
	modbus_poll_data[mb_brmenustate] = brstate;
	modbus_poll_data[mb_pbrmenustate] = pbrstate;
	modbus_poll_data[mb_loramodestate] = loramode;
	modbus_poll_data[mb_loramenustate] = lobstate;
	modbus_poll_data[mb_braketimer] = brake_timer;
	modbus_poll_data[mb_loratimer] = lora_timer;
	modbus_poll_data[mb_windsensor_toggle] = wind_sensor;
	modbus_poll_data[mb_wattmeter_toggle] = watt_meter;

	modbus_poll_data[mb_wattmeter_id] = wattmeter_slave_id;
	modbus_poll_data[mb_wattmeter_shuntval] = wattmeter_shunt_val;
	modbus_poll_data[mb_slave_id] = slave_id;


	modbus_poll_data[mb_vmax] = max_voltage_limit * 10;
	modbus_poll_data[mb_vmin] = min_voltage_limit * 10;
	modbus_poll_data[mb_wmax] = max_wind_limit * 10;
	modbus_poll_data[mb_wmin] = min_wind_limit * 10;

	if (charging())
		modbus_poll_data[mb_cur_state] = 1;
	if (braking)
		modbus_poll_data[mb_cur_state] = 2;
	if (phase_braking)
		modbus_poll_data[mb_cur_state] = 3;
	if (braking && (digitalRead(A5) == LOW))
		modbus_poll_data[mb_cur_state] = 4;
	if (phase_braking && (digitalRead(A5) == LOW))
		modbus_poll_data[mb_cur_state] = 5;
	if (voltage_error)
		modbus_poll_data[mb_volt_err] = 1;
	else
		modbus_poll_data[mb_volt_err] = 0;
	modbus_poll_data[mb_sens_err] = 0;
	bitWrite(modbus_poll_data[mb_sens_err],0, (wind_sensor_state == 0 && uint8_to_bool(wind_sensor) << 0));
	bitWrite(modbus_poll_data[mb_sens_err], 1, (wattmeter_error && uint8_to_bool(watt_meter)));
	bitWrite(modbus_poll_data[mb_sens_err], 2, (temp_error));
	bitWrite(modbus_poll_data[mb_sens_err], 3, (voltage_error));

	modbus_poll_data[mb_fan_state] = fan_state;
	
	for (int i = 0; i < mb_data_size; i++) { old_modbus_poll_data[i] = modbus_poll_data[i]; }
}
void update_own_data() {
	if ((modbus_poll_data[mb_vmax] > modbus_poll_data[mb_vmin]) && (modbus_poll_data[mb_vmax] != 0 && modbus_poll_data[mb_vmin] != 0)) {
		max_voltage_limit = (float)modbus_poll_data[mb_vmax] / 10;
		min_voltage_limit = (float)modbus_poll_data[mb_vmin] / 10;

	}
	if ((modbus_poll_data[mb_wmax] > modbus_poll_data[mb_wmin]) && (modbus_poll_data[mb_wmax] != 0 && modbus_poll_data[mb_wmin] != 0)) {
		max_wind_limit = (float)modbus_poll_data[mb_wmax] / 10;
		min_wind_limit = (float)modbus_poll_data[mb_wmin] / 10;
	}
	if ((modbus_poll_data[mb_loramenustate] >= PARAMON && modbus_poll_data[mb_loramenustate] <= PARAMAUTO)) {
		lobstate = modbus_poll_data[mb_loramenustate];
	}
	if ((modbus_poll_data[mb_loramodestate] >= PARAMON && modbus_poll_data[mb_loramodestate] <= PARAMOFF)) {
		loramode = modbus_poll_data[mb_loramodestate];
	}
	if ((modbus_poll_data[mb_brmenustate] >= PARAMON && modbus_poll_data[mb_brmenustate] <= PARAMAUTO)) {
		brstate = modbus_poll_data[mb_brmenustate];
	}
	if ((modbus_poll_data[mb_pbrmenustate] >= PARAMON && modbus_poll_data[mb_pbrmenustate] <= PARAMAUTO)) {
		pbrstate = modbus_poll_data[mb_pbrmenustate];
	}
	if ((modbus_poll_data[mb_wattmeter_shuntval] >= 0 && modbus_poll_data[mb_wattmeter_shuntval] <= 3)) {
		wattmeter_shunt_val = modbus_poll_data[mb_wattmeter_shuntval];
	}
	if ((modbus_poll_data[mb_wattmeter_id] >= 1 && modbus_poll_data[mb_wattmeter_id] <= 247)) {
		wattmeter_slave_id = modbus_poll_data[mb_wattmeter_id];
	}
	if ((modbus_poll_data[mb_slave_id] >= 1 && modbus_poll_data[mb_slave_id] <= 247)) {
		slave_id = modbus_poll_data[mb_slave_id];
	}
	brake_timer = modbus_poll_data[mb_braketimer];
	lora_timer = modbus_poll_data[mb_loratimer];
	wind_sensor = modbus_poll_data[mb_windsensor_toggle];
	watt_meter = modbus_poll_data[mb_wattmeter_toggle];

	for (int i = 0; i < mb_data_size; i++) {
		switch (i) {
		case mb_vmax:
		case mb_vmin:
		case mb_wmax:
		case mb_wmin:
		case mb_loramenustate:
		case mb_loramodestate:
		case mb_loratimer:
		case mb_braketimer:
		case mb_brmenustate: 
		case mb_pbrmenustate:
		case mb_windsensor_toggle:
		case mb_wattmeter_toggle:
		case mb_wattmeter_id:
		case mb_wattmeter_shuntval:
		case mb_slave_id:
			if (old_modbus_poll_data[i] != modbus_poll_data[i]) { save_flag = 1; 
			if (i == mb_wattmeter_shuntval)wattmeter_set_needed = true;
			}
			break;
		default: break;
		}
	}

}
void flash_errors() {
	if (millis() - flashing_period >= flashing_interval) {
		flashing_period = millis();
		flashing_bool = !flashing_bool;
	}
	if (flashing_bool) {
		if ((uint8_to_bool(voltage_al) && voltage_error) ||
			(wind_sensor_error && uint8_to_bool(wind_sensor) &&
				uint8_to_bool(wind_al))) {
			tone(buzzer_pin, 2000, 350);
		}
	}
}

void draw_voltage_mode() {
	if (VOLTAGE_MODE == 0) {
		u8g.setDrawColor(1);
		u8g.drawBox(4, 33, u8g.getStrWidth(LMODETEXT) + 3, 10);
		u8g.setDrawColor(0);
		u8g.drawStr(5, 40, LMODETEXT);
		u8g.setDrawColor(1);
		u8g.drawStr(20, 40, HMODETEXT);
	}
	else {
		u8g.setDrawColor(1);
		u8g.drawBox(19, 33, u8g.getStrWidth(HMODETEXT) + 3, 10);
		u8g.setDrawColor(0);
		u8g.drawStr(20, 40, HMODETEXT);
		u8g.setDrawColor(1);
		u8g.drawStr(5, 40, LMODETEXT);
	}
}

void draw_speed() {
	u8g.drawStr(25, 10, "Speed: ");
	if (wind_sensor_state == 1) {
		char speed_string[20];
		dtostrf(get_wind_speed(), 10, 1, speed_string);
		u8g.drawStr(20, 10, speed_string);
		u8g.drawStr(85, 10, "m/s");
	}
}

void draw_value(uint8_t val) {
	char* val_string[5] = { "Voltage", "Current", "Power", "Energy", "Br.Temp" };

	char* unit_string[5] = { "V", "A", "KW", "KWH", "C" };
	char buf[20];
	uint8_t dtoval = 5;
	if (watt_meter) {
		switch (val) {
		case val_VOLTAGE:
			dtostrf(get_voltage(), 5, 1, buf);
			dtoval = 5;
			break;
		case val_CURRENT:
			if (!wattmeter_error) {
				dtostrf(get_current() / 100, 5, 2, buf);
				dtoval = 1;
			}
			else {
				memset(buf, 0, sizeof(buf));
				strcat(buf, "ERR");
				dtoval = 5;
			}
			break;
		case val_KWH:
			if (!wattmeter_error) {
				dtostrf(get_wh() / 1000, 15, 3, buf);
				dtoval = 15;
			}
			else {
				memset(buf, 0, sizeof(buf));
				strcat(buf, "ERR");
				dtoval = 1;
			}
			break;
		case val_W:
			if (!wattmeter_error) {
				dtoval = 15;
				dtostrf(get_w() / 10000, 15, 3, buf);
			}
			else {
				memset(buf, 0, sizeof(buf));
				strcat(buf, "ERR");
				dtoval = 1;
			}
			break;
		case val_TEMP:
			float tmmp = get_temperature();
			if (temp_error) {
				memset(buf, 0, sizeof(buf));
				strcat(buf, "   ERROR ");
			}
			else {
				dtostrf(get_temperature(), 6, 2, buf);
			}
			dtoval = 6;
			break;
		}
	}
	else {
		switch (val) {
		case val_VOLTAGE:
			dtostrf(get_voltage(), 5, 1, buf);
			break;
		case val_TEMP:
			if (temp_error) {
				memset(buf, 0, sizeof(buf));
				strcat(buf, "   ERROR ");
			}
			else {
				dtostrf(get_temperature(), 6, 2, buf);
			}
			break;
		}
	}
	uint8_t val_x = 4;
	u8g.drawStr(val_x, 60, val_string[val]);
	val_x++;
	val_x += u8g.getStrWidth(val_string[val]);
	val_x = val_x - (dtoval == 15) * u8g.getStrWidth("0") * 10;
	u8g.drawStr(val_x, 60, buf);
	val_x++;
	val_x += u8g.getStrWidth(buf);
	u8g.drawStr(val_x, 60, unit_string[val]);
}

bool isBetween(float x,
	float llimit,
	float hlimit,
	bool llinc = true,
	bool hlinc = true) {
	bool b1 = false, b2 = false;
	if (llinc)
		b1 = (x >= llimit);
	else
		b1 = (x > llimit);
	if (hlinc)
		b2 = (x <= hlimit);
	else
		b2 = (x < hlimit);
	return b1 && b2;
}

void modbus_config() {
	mb_master.setTimeOut(500);
	mb_master.begin(1, sensor_serial);
	mb_master.preTransmission(master_pre_tranmission);
	mb_master.postTransmission(master_post_transmission);
	modbus_serial.begin(modbus_rs_baud, SERIAL_8N2);
	sensor_serial.begin(9600, SERIAL_8N1);
	slave.setID(slave_id);
	slave.start();
}

void startup_config() {

	VOLTAGE_MODE = get_voltage_mode();
	eeprom_control("READ");
#ifdef FAC_DEF
	set_factory_values();
#endif  // FAC_DEF

	if (!param_validity_check()) {
		//Serial.println("INVALID VALUES,RESETTING...");
		set_factory_values();
	}
	modbus_config();
}

bool param_validity_check() {
	for (uint8_t i = 0; i < 4; i++) {
		if (isnan(max_min_array[i]) || max_min_array[i] == 0.0)
			return false;
	}
	return true;
}

bool charging() {
	return (!phase_braking && !braking && !lora_pb_flag);
}

void reset_brake_before_pb_timer() {
	pb_after_brake_period = millis();
}

void reset_lora_timer() {
	lora_pb_period = millis();
}

void braking_func() {
	if (!phase_braking && !after_pb_braking) {
		if (enableresbreakcondition()) {
			braking = true;
			autoresbreak = true;
		}
		if (disableresbreakcondition()) {
			braking = false;
			autoresbreak = false;
		}
	}
	else {
		if (phase_braking) {
			braking = false;
		}
	}
}

void phase_braking_func() {
	if (uint8_to_bool(wind_sensor)) {
		if (enablephasebreakcondition() && !phase_braking && !after_pb_braking && !lora_pb_flag) {
			if (loramode == PARAMON && lobstate == PARAMAUTO) {
				reset_lora_timer();
				lora_pb_flag = true;
			}
			else {
				reset_brake_before_pb_timer();
				after_pb_braking = true;
			}
		}
		if (disablephasebreakcondition()) {
			phase_braking = false;
			lora_pb_flag = false;
			after_pb_braking = false;
			autopbreak = false;
			if (autoresbreak) {
				autoresbreak = false; braking = enableresbreakcondition();
			}
		}

		if (lora_pb_flag) {
			if (millis() - lora_pb_period >= (unsigned long)lora_timer * 1000) {
				reset_brake_before_pb_timer();
				after_pb_braking = true;
				lora_pb_flag = false;
			}
		}

		if (after_pb_braking) {
			braking = true;
			if (millis() - pb_after_brake_period >= (unsigned long)brake_timer * 1000) {
				phase_braking = true;
				after_pb_braking = false;
			}
		}

	}
	else {
		phase_braking = false;
		lora_pb_flag = false;
		after_pb_braking = false;
		autopbreak = false;
		if (autoresbreak) {
			autoresbreak = false; braking = enableresbreakcondition();
		}
	}
}

bool error_mode() {
	switch (VOLTAGE_MODE) {
	case 0:
		if (get_voltage() <= BAT_LVOLTAGE_LOW - 0.1)
			voltage_error = true;
		else
			voltage_error = false;
		break;
	case 1:
		if (get_voltage() <= BAT_HVOLTAGE_LOW - 0.1)
			voltage_error = true;
		else
			voltage_error = false;
		break;
	}
	wind_sensor_error = (wind_sensor_state == 0);
	return (wind_sensor_error || voltage_error);
}

float get_current() {
	return (float)modbus_poll_data[mb_current];
}

float get_w() {
	uint32_t ret_w = ((uint32_t)modbus_poll_data[mb_w_h] << 16 |
		(uint32_t)modbus_poll_data[mb_w_l]);
	return float(ret_w);
}

float get_wh() {
	uint32_t ret_wh = ((uint32_t)modbus_poll_data[mb_wh_h] << 16 |
		(uint32_t)modbus_poll_data[mb_wh_l]);
	return float(ret_wh);
	;
}

float get_temperature() {
	float R1 = 10000;
	float logR2, R2, T, Tc, Tf;
	float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;
	int Vo = analogRead(temp2_pin);
	R2 = R1 * (1023.0 / (float)Vo - 1.0);
	logR2 = log(R2);
	T = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2));
	Tc = T - 273.15;
	if (Tc < -100||Tc>500) {
		temp_error = true;
	}
	else
		temp_error = false;
	return Tc;
};

float get_voltage() {
	if (instant) {
		for (sample_count = 0; sample_count < max_samples; sample_count++) {
			voltage_sample_total += analogRead(voltage_read_pin);
			delay(max_sample_period);
		}
		sampled_voltage = (float)voltage_sample_total / max_samples;
	
		voltage_sample_total = 0;
		sample_count = 0;
	}
	else {
		if (sample_count < max_samples) {
			if (millis() - voltage_sample_period >= max_sample_period) {
				voltage_sample_period = millis();
				sample_count++;
				voltage_sample_total += analogRead(voltage_read_pin);
			}
		}
		else {
			sampled_voltage = (float)voltage_sample_total / max_samples;
			sample_count = 0;
			voltage_sample_total = 0;
		}
	}

	return mapfloat(sampled_voltage, mapVal1, mapVal2, mapVal3, mapVal4);
}

float get_wind_speed() {
	if (first_start ||
		(wsensqueue &&
			millis() - wind_sensor_poll_period >= wind_sensor_read_period)) {
		mb_master.clearResponseBuffer();
		mb_master.clearTransmitBuffer();
		first_start = false;
		rs_de = sensor_rs_de;
		wind_sensor_poll_period = millis();
		wperiod = millis();
		sensor_serial.begin(9600, SERIAL_8N1);
		mb_master.begin(1, sensor_serial);
		uint8_t result = mb_master.readHoldingRegisters(0, 1);

		if (result == mb_master.ku8MBSuccess) {
			wperiod = millis();
			wind_sensor_error_count = 0;
			wind_sensor_state = 1;
			modbus_poll_data[0] = mb_master.getResponseBuffer(0);
		}
		else {
			wind_sensor_error_count++;
			if (wind_sensor_error_count >= max_sensor_error_count) {
				wind_sensor_error_count = max_sensor_error_count;
				wind_sensor_state = 0;
				after_error = true;
			}
		}
	}
	wmeterqueue = true;
	if (wind_sensor_state == 0) {
		modbus_poll_data[0] = 0xFFFF;
		return -1;
	}
	return (float)(modbus_poll_data[0]) / 10;
}

void update_wattmeter_vals() {
	mb_master.clearResponseBuffer();
	mb_master.clearTransmitBuffer();
	rs_de = sensor_rs_de;
	sensor_serial.end();
	sensor_serial.begin(9600, SERIAL_8N2);
	mb_master.begin(wattmeter_slave_id, sensor_serial);
	uint8_t result = mb_master.readInputRegisters(1, 5);
	wdt_reset();
	if (result == mb_master.ku8MBSuccess) {
		wattmeter_error = false;
		for (int i = mb_current; i <= mb_wh_h; i++) {
			modbus_poll_data[i] = mb_master.getResponseBuffer(i - mb_current);
		}
	}
	else {
		wattmeter_error = true;
		for (int i = mb_current; i <= mb_wh_h; i++) {
			modbus_poll_data[i] = 0;
		}
	}
}

uint8_t get_voltage_mode() {
	if (get_voltage() > AUTO_MODE_DIFF_VOLTAGE) {
		instant = false;
		return 1;
	}
	instant = false;
	return 0;
}

void draw_errors() {
	if (flashing_bool) {
		if (voltage_error) {
			u8g.drawStr(110, 10, "E0");
		}
		if (uint8_to_bool(wind_sensor) && wind_sensor_error)
			u8g.drawStr(110, 20, "E1");
	}
}

void draw_performance_menu() {
	u8g.setFontPosBaseline();
	u8g.setFontMode(0);
	u8g.setDrawColor(1);
	u8g.setFont(u8g2_font_6x12_tr);
	float battery_percent = 0;
	uint8_t battery_level = 0;
	u8g.drawXBMP(0, 0, 32, 32, wind1);     /*DRAW WIND TURBINE*/
	u8g.drawXBMP(45, 15, 25, 28, battery); /*DRAW BATTERY*/

	if ((isBetween(get_voltage(), BAT_LLEVELVOLTAGE_LOW,
		BAT_LLEVELVOLTAGE_HIGH) &&
		get_voltage_mode() == 0) ||
		(isBetween(get_voltage(), BAT_HLEVELVOLTAGE_LOW,
			BAT_HLEVELVOLTAGE_HIGH) &&
			get_voltage_mode() == 1)) {
		switch (VOLTAGE_MODE) {
		case 0:
			battery_percent = mapfloat(get_voltage(), BAT_LVOLTAGE_LOW,
				BAT_LVOLTAGE_HIGH, 20, 100);
			break;
		case 1:
			battery_percent = mapfloat(get_voltage(), BAT_HVOLTAGE_LOW,
				BAT_HVOLTAGE_HIGH, 20, 100);
			break;
		}

		if (battery_percent < 20)
			battery_percent = 0;
		battery_level = (int)battery_percent / 20;
		if (battery_level > 5)
			battery_level = 5;
		for (uint8_t i = 0; i < battery_level + 1; i++) {
			u8g.drawXBMP(47, 37 - i * 4, 21, 3, battery2); /*FILL BATTERY*/
		}
	}

	if (braking) {
		u8g.drawXBMP(90, 25, 27, 13, resistor1); /*DRAW BRAKING RESISTOR*/
	}
	if (phase_braking) {
		u8g.drawXBMP(90, 25, 25, 27, resistor3); /*DRAW PBRAKING RESISTOR*/
	}
	char percent_string[10];
	if (battery_percent > 100)
		battery_percent = 100;
	dtostrf(battery_percent, 4, 0, percent_string);
	draw_value(selected_value);
	if (selected_value == val_VOLTAGE) {
		u8g.drawStr(97, 60, percent_string);
		u8g.drawStr(122, 60, "%");
	}
	draw_voltage_mode();
	if (uint8_to_bool(wind_sensor))
		draw_speed();
	if (error_mode())
		draw_errors();
}

uint16_t filter_analog1(uint16_t input) {
	static uint16_t result = 0;
	float alfa = 0.7;
	result = result * alfa + input * (1 - alfa);
	return result;
}
