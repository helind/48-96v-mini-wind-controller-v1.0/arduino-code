#include <RadioLib.h>
#include <avr/wdt.h>

const uint8_t NSS_PIN = A1;
const uint8_t DIO0_PIN = 2;
const uint8_t RESET_PIN = A3;
const uint8_t DIO1_PIN = 3;
SX1276 radio = new Module(NSS_PIN, DIO0_PIN, RESET_PIN, DIO1_PIN);

const uint8_t PWM_L = 9;
const uint8_t PWM_R = 10;

const uint8_t voltageReadPin = A0;


bool first_start = true;
unsigned long error_period = 0;
unsigned long error_max = 10000;
bool comm_error = false;
bool voltage_error = false;
volatile bool receivedFlag = false;
volatile bool enableInterrupt = true;
volatile bool brakingFlag = false;

uint16_t voltageVal = 0;

const float minVoltageLimit = 10.5;
const float maxVoltageLimit = 11;
String str;
unsigned long debug_millis = 0, debug_period = 1000;
bool debug_time = true;
void setup() {
  MCUSR = 0;
  wdt_disable();
  noInterrupts();
  pinMode(PWM_L, OUTPUT);
  pinMode(PWM_R, OUTPUT);
  timer1_config();
  Serial.begin(19200);
  interrupts();
  wdt_enable(WDTO_4S);
  int state = radio.begin();
  if (state == ERR_NONE) {
    radio.setDio0Action(setFlag);
    state = radio.startReceive();
  }
  else {
    Serial.print("Lora Error! Code: "); Serial.println(state);
    delay(2000);
    Serial.print("REBOOTING");
    while (1);
  }
  str.reserve(10);
}

void loop() {
  voltageVal = analogRead(voltageReadPin);
  float cur_volt = mapfloat(voltageVal, 661, 693, 10.5, 11);
  if (cur_volt <= minVoltageLimit) {
    voltage_error = true;
  }
  else {
    if (cur_volt >= maxVoltageLimit) {
      voltage_error = false;
    }
  }
  comm_error = (millis() - error_period >= error_max);

  communicationTask();
  if (!voltage_error && !comm_error) {
    if (brakingFlag) {
      closeTurbine();
    }
    else {
      openTurbine();
    }
  }
  else {
    closeTurbine();

  }
  if(millis()-debug_millis>=debug_period){
    debug_millis = millis();
        if (voltage_error) {
      Serial.println("VOLTAGE ERROR,CLOSING");
    }
    if (comm_error) {
      Serial.println("COMM ERROR, CLOSING");
    }

    }
  wdt_reset();
}
void communicationTask() {

  if (receivedFlag) {
    enableInterrupt = false;
    receivedFlag = false;
    Serial.print(F("[SX127x] RSSI:\t\t")); Serial.print(radio.getRSSI()); Serial.println(F(" dBm"));
    int state = radio.readData(str);
    if (state == ERR_NONE) {
      Serial.print(F("[SX127x] DATA:\t\t")); Serial.println(str);
      if (strstr(str.c_str()  , "PB1")) {
        brakingFlag = true;
        Serial.println("CLOSING");
        error_period = millis();
      }
      else if (strstr(str.c_str()  , "NC0")) {
        Serial.println("OPENING");
        brakingFlag = false;
        error_period = millis();
      }
    }
    else {
      Serial.print("Error! Code: "); Serial.println(state);
    }
    wdt_reset();

    radio.startReceive();

    enableInterrupt = true;
  }
}

void setFlag(void)
{
  if (!enableInterrupt)
  {
    return;
  }

  receivedFlag = true;
}

void closeTurbine() {
  set_pwm(10, 0);
  set_pwm(9, 95);
}
void openTurbine() {
  set_pwm(9, 0);
  set_pwm(10, 95);
}
void set_pwm(uint8_t pin, uint8_t pwm_val)
{
  uint32_t val = 0;

  switch (pin)
  {
    case PWM_L:
      val = (ICR1 * pwm_val) / 100;
      OCR1A = (uint16_t) val;
      break;
    case PWM_R:
      val = (ICR1 * pwm_val) / 100;
      OCR1B = (uint16_t) val;
      break;

  }
}
void timer1_config() {
  TCCR1A = (1 << COM1A1) | (1 << COM1B1) | (1 << WGM11);
  TCCR1B = (1 << WGM13) | (1 << CS10);
  ICR1 = 320;
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
