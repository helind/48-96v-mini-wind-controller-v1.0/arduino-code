#include <RadioLib.h>
#include <avr/wdt.h>
#define lora1278


const uint8_t NSS_PIN =A1;
const uint8_t DIO0_PIN = 2;
const uint8_t RESET_PIN = A3;
const uint8_t DIO1_PIN = 3;

const uint8_t BRAKE_FLAG_PIN = 5;
#ifdef lora1276
SX1276 radio = new Module(NSS_PIN, DIO0_PIN, RESET_PIN, DIO1_PIN);
#endif
#ifdef lora1278
SX1278 radio = new Module(NSS_PIN, DIO0_PIN, RESET_PIN, DIO1_PIN);
#endif
uint8_t byteArr[3] = {0};
int transmissionState = ERR_NONE;
void setup() {
   MCUSR = 0;
  wdt_disable();
 
  pinMode(BRAKE_FLAG_PIN,INPUT_PULLUP);
  int state = radio.begin();
  if (state == ERR_NONE) {
  } else {

    while (true);
  }

  radio.setDio0Action(setFlag);
 Serial.begin(9600);
  transmissionState = radio.startTransmit("Hello World!");
  wdt_enable(WDTO_4S);
  /* if (radio.setFrequency(433.5) == ERR_INVALID_FREQUENCY) {
    Serial.println(F("Selected frequency is invalid for this module!"));
    while (true);
  }*/
 
}

// flag to indicate that a packet was sent
volatile bool transmittedFlag = false;
volatile bool enableInterrupt = true;
void setFlag(void) {
  if (!enableInterrupt) {
    return;
  }
  transmittedFlag = true;
}

void loop() {
  if (transmittedFlag) {
    enableInterrupt = false;
    transmittedFlag = false;
    if (transmissionState == ERR_NONE) {      Serial.println("SUCCESS");    }
    else {       Serial.println("FAIL");   }
    delay(1000);

    if (digitalRead(BRAKE_FLAG_PIN) == HIGH) {
      byteArr[0] = 'P';  byteArr[1] = 'B'; byteArr[2] = '1';
    }
    else {
      byteArr[0] = 'N';  byteArr[1] = 'C'; byteArr[2] = '0';
    }
    transmissionState = radio.startTransmit(byteArr, 3);
        if (transmissionState == ERR_NONE) {   
          Serial.println("SUCCESS");
          }
             else {       Serial.println("FAIL");   }
    wdt_reset();
    enableInterrupt = true;
  }
  wdt_reset();
}
